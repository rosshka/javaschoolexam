package com.tsystems.javaschool.tasks.zones;

import com.sun.org.apache.xml.internal.utils.ListingErrorHandler;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.*;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        if (requestedZoneIds.size() == 1) return false;
        Map<Integer, List<Integer>> zoneSt = new HashMap<Integer, List<Integer>>(){};
        for(Zone curr : zoneState) zoneSt.put(curr.getId(), curr.getNeighbours());
        for(Map.Entry<Integer, List<Integer>> entry : zoneSt.entrySet()){
            for(Integer curr : entry.getValue()){
                if(!zoneSt.get(curr).contains(entry.getKey())){
                    List<Integer> toRewrite = new LinkedList<>();
                    toRewrite.addAll(zoneSt.get(curr));
                    toRewrite.add(entry.getKey());
                    zoneSt.put(curr, toRewrite);
                }
            }
        }

        Collection<List<Integer>> output = permute(requestedZoneIds);
        List<Boolean> flags = new LinkedList<Boolean>();
        
        boolean currCorrect;
        for(List<Integer> currPerm : output) {
            currCorrect = true;
            for (int i = 0; i < currPerm.size() - 1; i++) {
                int curr = currPerm.get(i);
                int next = currPerm.get(i + 1);
                List<Integer> currNbs = zoneState.get(curr - 1).getNeighbours();
                List<Integer> nextNbs = zoneState.get(next - 1).getNeighbours();
                if (!currNbs.contains(next)) {
                    if (!nextNbs.contains(curr)) {
                        flags.add(false);
                        currCorrect = false;
                        break;
                    }
                }
            }
            if(currCorrect) return true;
        }

        if(flags.contains(true)) return true;
        return false;
    }
    public Collection<List<Integer>> permute(Collection<Integer> input) {
        Collection<List<Integer>> output = new ArrayList<List<Integer>>();
        if (input.isEmpty()) {
            output.add(new ArrayList<Integer>());
            return output;
        }
        List<Integer> list = new ArrayList<Integer>(input);
        int head = list.get(0);
        List<Integer> rest = list.subList(1, list.size());
        for (List<Integer> permutations : permute(rest)) {
            List<List<Integer>> subLists = new ArrayList<List<Integer>>();
            for (int i = 0; i <= permutations.size(); i++) {
                List<Integer> subList = new ArrayList<Integer>();
                subList.addAll(permutations);
                subList.add(i, head);
                subLists.add(subList);
            }
            output.addAll(subLists);
        }
        return output;
    }

}
