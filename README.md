# README #

This is a repo with T-Systems Java School preliminary examination tasks.

The exam includes 2 tasks to be done: [Pyramid](/tasks/Pyramid.md), [Zones](/tasks/Zones.md)

### Result ###

* Author name : Androsov Vladimir (rosshka@gmail.com)
* Codeship : [ ![Codeship Status for rosshka/javaschoolexam](https://app.codeship.com/projects/22c26500-7774-0135-8810-6e5f001a2e3c/status?branch=master)](https://app.codeship.com/projects/244691)